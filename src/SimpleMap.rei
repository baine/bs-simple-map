type t('a, 'b);
let make: unit => t('a, 'b);
let remove: (t('a, 'b), 'a) => t('a, 'b);
let add: (t('a, 'b), 'a, 'b) => t('a, 'b);
let lookup: (t('a, 'b), 'a) => option('b);

let entries: t('a, 'b) => list(('a, 'b));
let keys: t('a, 'b) => list('a);
let values: t('a, 'b) => list('b);
