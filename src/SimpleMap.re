type t('a, 'b) = list(('a, 'b));
let make: unit => t('a, 'b) = () => [];

let rec filterHelper = (accumulated, m, predicate) =>
  switch (m) {
  | [] => accumulated
  | [item, ...rest] =>
    filterHelper(
      if (predicate(item)) {
        [item, ...accumulated];
      } else {
        accumulated;
      },
      rest,
      predicate,
    )
  };

let filter = (m, pred) => filterHelper([], m, pred);

exception Impossible;
let lookup = (m, k) =>
  switch (filter(m, ((k', _)) => k == k')) {
  | [] => None
  | [(_, v)] => Some(v)
  | [_, ..._] => raise(Impossible)
  };

let remove = (m, k) => filter(m, ((k', _)) => k' != k);
let add = (m, k, v) => [(k, v), ...remove(m, k)];

let entries = m => m;
let keys = m => List.map(fst, m);
let values = m => List.map(snd, m);
